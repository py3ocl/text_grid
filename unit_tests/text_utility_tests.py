#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys

sys.path.append("..")

from text_utility import TextUtility

import unittest

class TestTextUtilityMethods(unittest.TestCase):

    def test_Split(self):

        # Test single character with/without space separator.
        strings = TextUtility.Split("A", [' '])
        self.assertTrue(strings == ['A'])

        strings = TextUtility.Split(" A", [' '])
        self.assertTrue(strings == ['A'])

        strings = TextUtility.Split("A ", [' '])
        self.assertTrue(strings == ['A'])

        strings = TextUtility.Split(" A ", [' '])
        self.assertTrue(strings == ['A'])

        # Test two characters with/without space separator.
        strings = TextUtility.Split("A B", [' '])
        self.assertTrue(strings == ['A', 'B'])

        strings = TextUtility.Split(" A B", [' '])
        self.assertTrue(strings == ['A', 'B'])

        strings = TextUtility.Split("A B ", [' '])
        self.assertTrue(strings == ['A', 'B'])

        strings = TextUtility.Split(" A B ", [' '])
        self.assertTrue(strings == ['A', 'B'])

        # Test three characters with/without space separator.
        strings = TextUtility.Split("A B C", [' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split(" A B C", [' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("A B C ", [' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split(" A B C ", [' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        # Test hyphen separator (anything but space)
        strings = TextUtility.Split("A-B-C", ['-'])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("-A-B-C", ['-'])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("A-B-C-", ['-'])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("-A-B-C-", ['-'])
        self.assertTrue(strings == ['A', 'B', 'C'])

        # Test space and hyphen separators
        strings = TextUtility.Split("A-B-C", ['-', ' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("-A-B-C", ['-', ' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("A-B-C-", ['-', ' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("-A-B-C-", ['-', ' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("A- B- C", ['-', ' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("- A- B- C", ['-', ' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("A- B- C- ", ['-', ' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        strings = TextUtility.Split("- A- B- C- ", ['-', ' '])
        self.assertTrue(strings == ['A', 'B', 'C'])

        # Test no searators found.
        strings = TextUtility.Split("A B C", ['-'])
        self.assertTrue(strings == ["A B C"])

if __name__ == '__main__':
    unittest.main()
