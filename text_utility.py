#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class TextUtility:

    ## Split a text string into strings, split by the provided separators.
    ## Note that separators is a list of characters, e.g. [' ', '\n']
    @staticmethod
    def Split(text, separators):
        pos = 0
        strings = []
        text_len = len(text)

        split_len = 0

        if separators and text:

            # Skip leading separators, always starting on a valid character.
            while pos < text_len and text[pos] in separators:
                pos += 1

            while pos < text_len:

                # Search for the next separator past the last found sequence of separators.

                end_pos = pos
                while end_pos < text_len and text[end_pos] not in separators:
                    end_pos += 1

                strings.append(text[pos:end_pos])

                pos = end_pos + 1
                while pos < text_len and text[pos] in separators:
                    pos += 1

        return strings
